# Kaldi analyzer - Gabriel Caferra

## How to install kaldi_utt package?
 
* Clone the whole repository from the following link:
https://gitlab.com/teklia/simplon/kaldi-analyzer-gabriel-caferra

* We strongly recommend that you run the package in pipenv virtual environment as some external modules are included:

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/kaldi-analyzer-gabriel-caferra$ pipenv install -d

```

## How to run modules from the package?

From your pipenv environment you can import kaldi_utt package, its dependent modules with relevant functions

### The easy way

You can now call 'kaldi-per-utt-parse' and 'kaldi-per-utt-report' directly from the console terminal!

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/
kaldi-per-utt-parse ../kaldi_per_utt/balsac/ ./TEKLIA.db

```

The console script 'kaldi-per-utt-parse' expects two command line arguments, the first one for the repository containing the per_utt textfiles.

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/
kaldi-per-utt-report ./TEKLIA.db ./reports/

```

The console script 'kaldi-per-utt-report' expects two command line arguments, the first one for the path to the database (created by 'kaldi-per-utt-parse'), and the second one for the repository where you want the reports to be stored. Reports are edited as HTML, CSV and JSON files.

### For test purposes

You can launch unit tests by running pytest at the root of the repository:

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/ pytest tests -v

```

You may want to test a single function within a module. Let's try with the module 'data_pipeline.py' which contains the 'pipeline_execution' function. The first argument of the function is a string representing the path to a textfile. The second one is an sqlite3.Connection with a string to a database. You will have to import sqlite3 and then run the function with suitable arguments:

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/kaldi-analyzer-gabriel-caferra$ python3
>>> import sqlite3
>>> import kaldi_utt.data_pipeline
>>> kaldi_utt.data_pipeline.pipeline_execution('../kaldi_per_utt/balsac/per_utt_test',sqlite3.Connection('./TEKLIA.db'))

```

## Repository structure

At the root of the repository you will find:

* 'pipfile' that allows you to create a pipenv environment, and automatically install the kaldi_utt package.
* 'setup.py' allows you to manually install the kaldi_utt package. In this case, you will have to run:

```bash

gabriel@Gabriel:~/Projets/Teklia/package_test/ pipenv install -e .

```

* 'SQL_as_dict' gathers all the queries that 'kaldi-per-utt-report' runs. You can add your own request provided the dictionary structure is maintained.

You will find additional repositories:

* 'doc' gathers all the information about the program structure and conception. You will find the complete pipeline for console scripts and explanations for functions working.

* 'kaldi_utt' gathers all modules needed in order to run the python executables.

* 'reports' gathers the reports for all the requests from the 'SQLqueries_as_dict'. You can choose your own repository by precising it as CLI argument.

* 'tests' gathers the files and modules for the unit tests and their relative fixtures.