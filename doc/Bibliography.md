# Bibliography

## Best choices for data visualisation

https://infogram.com/page/choose-the-right-chart-data-visualization
https://drawsql.app/ (pour la modélisation de bases de données)
https://app.diagrams.net/

## About bag of words technique

https://www.datacorner.fr/bag-of-words/
https://www.analyticsvidhya.com/blog/2019/08/how-to-remove-stopwords-text-normalization-nltk-spacy-gensim-python/

## How to record rows into a table without SQL injections attacks risks

https://stackoverflow.com/questions/12295605/how-to-insert-the-keys-and-values-of-python-dict-in-mysql-using-python

## How to skim through the files in a directory

https://waytolearnx.com/2019/04/comment-lister-tous-les-fichiers-dun-repertoire-en-python.html
https://askcodez.com/parcourir-les-fichiers-et-sous-dossiers-en-python.html

## How to create a mock for cli arguments script

https://stackoverflow.com/questions/43390053/setting-command-line-arguments-for-main-function-tests

## How to have a main module as a function and run it from the main module

https://dusty.phillips.codes/2018/08/13/python-loading-pathlib-paths-with-argparse/

## to hide code from jupyter notebook

jupyter nbconvert --no-input --to=html --post=serve my_notebook.ipynb

## To be sure that jupyter kernel takes account of modules installed

Example:
pipenv install ipykernel ipysheet pandas

## Using qgrid module within jupyter notebook

https://github.com/quantopian/qgrid

## How to get headers from an sql request in Python

https://code.adonline.id.au/sql-to-csv-via-python/

## How to read Dictionary from File in Python?

https://www.geeksforgeeks.org/how-to-read-dictionary-from-file-in-python/

## Manage the module logging for editing logfile

https://docs.python.org/3/library/logging.html#levels

## Easy way to generate html table

https://stackoverflow.com/questions/22874058/generate-html-table-from-python-dictionary

## How to define the scope of a fixture

https://stackoverflow.com/questions/35868244/managing-many-session-scoped-fixtures-in-pytest#35869601

## How to use a mark to inject a fixture?

https://stackoverflow.com/questions/50593556/pytest-how-to-use-a-mark-to-inject-a-fixture#50606690
https://gist.github.com/peterhurford/09f7dcda0ab04b95c026c60fa49c2a68

## How to set up the 'setup.py' module to create a Python package and a python executable

https://python-packaging.readthedocs.io/en/latest/minimal.html

## How to install setup.py to make executable ready for CLI

https://python-packaging-tutorial.readthedocs.io/en/latest/setup_py.html

## How to automatically test for package install

https://romain-clement.net/articles/python-packages-development-pipenv