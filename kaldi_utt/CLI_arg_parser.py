""" This module contains the function that manages CLI arguments and
verify that CLI arguments correspond to suitable inputs, for both 
main and report_edition modules 
"""

import argparse
from pathlib import Path
import os
import logging
import sqlite3

def main_arg_parser_function()-> (str,str):
    """ Captures the CLI arguments for the main.py module and tests if: 
        -the text file directory exists
        -the suitable files are present in the text file directory
        it returns the validated arguments as a tuple of strings
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_directory",
        type=Path,
        help="Path to the data directory",
    )

    parser.add_argument(
        "database_path",
        type=Path,
        help="Path to the database file",
    )

    p = parser.parse_args()
    

    db_path = str(p.database_path)
    text_file_dir = p.input_directory
    
    # checking text file directory
    if os.path.isdir(text_file_dir) != True:
        logging.error(f"{text_file_dir} is not an existing repository")
        raise ValueError()
    else:
        # checking suitable text files
        for text_file in os.listdir(text_file_dir):
            
            if ('per_utt_val' != text_file) and ('per_utt_train' != text_file) and ('per_utt_test' != text_file):
                logging.error(f"{text_file} does not seem to be a per utt_file")
                raise ValueError()
            
        
        return(text_file_dir,db_path)

def report_arg_parser_function()-> (str,str):
    """ Captures the CLI arguments for the report_edition.py module and tests if: 
        -the database exists
        -the repository to the reports already exists
        it returns the validated arguments as a tuple of strings
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "database_path",
        type=Path,
        help="Path to the database file",
    )
        
    parser.add_argument(
        "report_directory",
        type=Path,
        help="Path to the report directory",
    )

    p = parser.parse_args()
    str_db_path=str(p.database_path)
    str_path_to_the_reports=str(p.report_directory)
    
    # checking that the database exists
    if not os.path.isfile(str_db_path):
        logging.error(f"No such database {str_db_path}")
        raise ValueError()
    else:
        # checking that the report directory exists
        if os.path.isdir(str_path_to_the_reports) != True:
            logging.error(f"{str_path_to_the_reports} is not an existing repository")
            raise ValueError()
        else:
            return(str_db_path,str_path_to_the_reports)