""" This module contains the 'pipeline_execution' function that 
    runs the whole data pipeline.
"""
import os.path
import sqlite3

from kaldi_utt.parser_functions import txt_file_parser, word_parser
from kaldi_utt.flattener import parsed_txt_file_flattener
from kaldi_utt.analytic_model import tables_creation, analytical_model_creation

def pipeline_execution(txt_file:str,con: sqlite3.Connection) -> None:
    """This function calls the following functions with given arguments:
        -table_creation from analytic_model module,
        -txt_file_parser from parser_functions module,
        -parsed_txt_file_flattener from flattener module
        -analytical_model_creation
    """
    
    # starting the pipeline
    analytical_model_creation(
        word_parser(
            parsed_txt_file_flattener(
                txt_file_parser(
                    txt_file
                    )
            )
        ,con),
        con)
    
