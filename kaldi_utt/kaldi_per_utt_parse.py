""" This module contains the 'main' function which is in charge of running 
    the whole pipeline
"""
import os
import sqlite3
import logging
import time

from kaldi_utt.analytic_model import tables_creation
from kaldi_utt.data_pipeline import pipeline_execution
from kaldi_utt.CLI_arg_parser import main_arg_parser_function


def main():
    """ This function is in charge of:
        -setting the timer,
        -setting logging configuration,
        -getting CLI arguments from CLI_arg_parser.py module
        -running the pipeline for all per_utt text files
    """
    # starting timer
    start = time.time()

    # configurating log as console message
    logging.basicConfig(filename=None, level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(name)s %(message)s')

    # calling main_arg_parser_function to get the checked CLI arguments
    args = main_arg_parser_function()
    text_file_dir = args[0]

    # connecting to the database
    TEKLIA_con = sqlite3.Connection(args[1])
    tables_creation(TEKLIA_con)

    # running pipeline for each file in the checked repository
    for text_file in os.listdir(text_file_dir):

        path_to_the_txt_file = str(text_file_dir) + '/' + text_file
        pipeline_execution(path_to_the_txt_file, TEKLIA_con)

    # closing database connection
    cur = TEKLIA_con.cursor()
    cur.close()

    # calculating processing time
    end = time.time()
    time_taken = round(float(end-start), 1)
    ms_taken = str(time_taken)
    logging.info(
        f"{ms_taken}s to parse and record per_utt files into {str(args[1])} database")

if __name__ == "__main__":
    main()
