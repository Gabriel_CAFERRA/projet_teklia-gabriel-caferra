import logging

def parsed_txt_file_flattener(dict_from_txt_file: dict) -> dict:
    """ This function gets:
        -the dictionary of the parsed textfiles with the following keys:
        'learning_step','work_reference','set' filled with lists of strings
        -writes the reference set and hypothesis set into the same row,
        alongside with their common references and summary. 
        It returns a dictionary with the 6 following keys:
        ['learning_step','work_reference','reference_set','hypotesis_set','op', 'CSID']
        Error is raised when input dictionary has irrelevant keys
    """
    # testing that input dictionary has the right keys
    if list(dict_from_txt_file.keys()) != ['learning_step','work_reference','set_type','set']:
        logging.error('input dictionary has not the suitable keys')
        raise ValueError()
    else:
        dict_val = dict.fromkeys(['learning_step','work_reference',
        'reference_set','hypothesis_set','op', 'CSID']) 

        # initializing keys as lists
        for key in dict_val.keys():
            dict_val[key]=list() 
    
        for i in range(0, len(dict_from_txt_file['learning_step']), 4):

            dict_val['learning_step'].append(dict_from_txt_file['learning_step'][i])
            
            dict_val['work_reference'].append(dict_from_txt_file['work_reference'][i])
            
            dict_val['reference_set'].append(dict_from_txt_file['set'][i])
            
            dict_val['hypothesis_set'].append(dict_from_txt_file['set'][i+1])

            dict_val['op'].append(dict_from_txt_file['set'][i+2])

            dict_val['CSID'].append(dict_from_txt_file['set'][i+3])
    
    return(dict_val)