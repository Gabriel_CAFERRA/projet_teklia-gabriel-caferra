""" This module contains 2 functions:
    -'tables_creation', that creates of the 'tokens' and 'lines' tables
    -'analytical_model_creation', that inserts records into the two tables
"""
import sqlite3

def tables_creation(con: sqlite3.Connection) -> None:
    """ This function creates both tokens and lines tables
    """
    # 'lines' table creation
    cur = con.cursor()
    cur.execute(""" DROP TABLE IF EXISTS lines;""")
    con.commit()
    cur.execute(""" CREATE TABLE lines(
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "learning_step" STRING,
    "work_reference" STRING,
    "reference_set" STRING,
    "hypotesis_set" STRING,
    "op" STRING,
    "CSID" STRING
        );""")
    con.commit()
    # 'tokens' table creation
    cur.execute("""DROP TABLE IF EXISTS tokens;""")
    con.commit()
    cur.execute("""CREATE TABLE tokens (
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "word_position" INTEGER,
        "word_of_reference" STRING,
        "word_found" STRING,
        "matching_type" STRING,
        "line_id" INTEGER,
        FOREIGN KEY ("line_id") REFERENCES lines ("id")
        );""")
    con.commit()


def analytical_model_creation(lists_tuple: tuple, con: sqlite3.Connection) -> None:
    """ Creates the relational database:
        - 'lines' table is filled with the first given tuple element returned by 
        word_parser function from the parser_functions module,
        - 'tokens' tables is filled with the second given tuple returned by 
        word_parser function from the parser_functions module,
        - the foreign key 'line_id' from 'tokens' table refers to its line reference
        in the 'lines' table. 'line_id' is provided by 'word_parser' function in the 
        parser_functions module, when filling the second tuple. 
    """
    
    cur = con.cursor()
    # filling 'lines' table
    cur.executemany("""INSERT INTO lines(
        learning_step,
        work_reference,
        reference_set,
        hypotesis_set,
        op,
        CSID
        ) 
        VALUES (?,?,?,?,?,?)""", lists_tuple[0])
    con.commit()
    # filling 'tokens' table
    cur.executemany("""INSERT INTO tokens(
        word_position,
        word_of_reference,
        word_found,
        matching_type,
        line_id
    ) VALUES (?,?,?,?,?)""", lists_tuple[1])
    
    con.commit()
    
