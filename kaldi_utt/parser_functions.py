""" This module contains:
    -txt_file_parser, that parses the lines from the textfile
    and write parsed lines as new values of a dictionary
    -word_parser, that parses each value of the reference and hypothesis sets into
    single words, each word and its full reference are recorded in a tuple.
"""
import os.path
import logging
import re
import sqlite3

def txt_file_parser(opened_file: str) -> dict:
    """This function takes charge of parsing the per_utt files in 3 groups: 
        'work_reference','set_type','set',
        each single group is written as a new value of a list stored in relevant 
        dictionary key. The first key of the dictionary contains the learning_step
        related to the textfile.
        Raises an error if:
        - the number of lines in the per_utt files doesn't ensure data integrity 
        - the parsing pattern doesn't match any per_utt file line
    """

    #checking for suitable number of lines
    with open(opened_file, 'r') as txt_file:
        if (sum(1 for line in txt_file) % 4) != 0:
            logging.error(f"unsuitable number of lines in the textfile: {opened_file} ")
            raise ValueError()
        else:
            with open(opened_file, 'r') as txt_file:
                
                fromTXT_dict = dict.fromkeys(['learning_step', 'work_reference',
                                              'set_type', 'set'])
                
                for key in fromTXT_dict.keys():
                    fromTXT_dict[key] = list()
                               
                # lazy regular expression ([^ ]*) ([^ ]*) (.*)

                regex = re.compile(
                    r'(?P<work_reference>[^ ]*) (?P<set_type>[^ ]*) (?P<set>.*)')

                for line in txt_file:

                    matching = re.match(regex, line)

                    # checking that the regular expression matches
                    if matching is None:
                        logging.error(f"regular expression failed to parse txt_file at line: {line}")       
                        raise ValueError()
                        
                    else:
                        
                        learningstep = opened_file.split('/')[-1]

                        if 'val' in learningstep:
                                
                            fromTXT_dict['learning_step'].append('val')

                        if 'test' in learningstep:
                        
                            fromTXT_dict['learning_step'].append('test')

                        if 'train' in learningstep:
                                
                            fromTXT_dict['learning_step'].append('train')

                        work_reference, set_type, set = matching.groups()
                        
                        # All matching groups written in the respective key of the dictionary
                        fromTXT_dict['work_reference'].append(work_reference)
                        fromTXT_dict['set_type'].append(set_type)
                        fromTXT_dict['set'].append(set)
    
    return fromTXT_dict


def word_parser(dict_transposed: dict, con: sqlite3.Connection) -> (list, list):
    """ This function gets the dictionary of the parsed textfiles, 
        splits the reference and hytpotesis sets into word of reference and relative word found.
        It returns two lists of tuples as the schema below:
        [(learning_step1,work_reference1,reference_set1,hypothesis_set1,op1,CSID1),..],
        [(word_of_reference1,word_found1,matching_type1,line_id1),...]
        The function checks that the input dictionary has the suitable keys
    """
    
    # verifying that the input dictionary as the suitable keys
    if list(dict_transposed.keys()) != ['learning_step','work_reference','reference_set','hypothesis_set','op','CSID']:
        logging.error('input dictionary has not the suitable keys')
        raise ValueError()
    else:
        cur = con.cursor()
        # In order to get the correct line_id foreign key, the function asks the table 'lines'
        # for the current line_id, and increments from this current line_id
        cur.execute("""SELECT MAX(ID) FROM lines;""")
        max_id=cur.fetchone()[0]
        
        if max_id is None:
            max_id=0
        else:
            max_id = int(max_id)
        
        # initializing both lists of tuples
        tuple_list_to_lines = []
        tuple_list_to_tokens = []

        for i in range(len(dict_transposed['learning_step'])):
            
            # creating tuple that fills one row in 'lines' table
            data_tuple_to_lines = (
                    dict_transposed['learning_step'][i],
                    dict_transposed['work_reference'][i],
                    dict_transposed['reference_set'][i],
                    dict_transposed['hypothesis_set'][i],
                    dict_transposed['op'][i],
                    dict_transposed['CSID'][i]
                )

            # recording the tuple in the relevant list
            tuple_list_to_lines.append(data_tuple_to_lines)

            for list_index in range(len(dict_transposed['reference_set'][i].split())):
                
                # creating tuple that fills one row in 'tokens' table
                # each triplets of 'reference_set', 'hypothesis_set', 'op' are split
                # into single words and written as a new record as word_of_reference, word_found
                # and matching type
                data_tuple_to_tokens = (
                    list_index,
                    dict_transposed['reference_set'][i].split()[list_index],
                    dict_transposed['hypothesis_set'][i].split()[list_index],
                    dict_transposed['op'][i].split()[list_index],
                    i+1+max_id
                )

                tuple_list_to_tokens.append(data_tuple_to_tokens)

        return(tuple_list_to_lines,tuple_list_to_tokens)
