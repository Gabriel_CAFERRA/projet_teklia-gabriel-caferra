""" This module contains the functions responsible for queries execution 
    and their edition.
"""
import sqlite3
import csv
import json
import logging

def query_execution_function(query_as_string: str, con: sqlite3.Connection) -> (list,list):
    """ This function gets a given string as an SQLite query to execute through
        the specified connection.
        Error is raised if the string doesn't represent a valid SQL query
        Return fields names and query results as 2 lists.
    """

    cur = con.cursor()
    try:
        # executing the string representing SQL query
        cur.execute(query_as_string) 
        
    except sqlite3.OperationalError:
        logging.error("Invalid SQL query")
        raise ValueError()
    
    # there is no sqlite3.OperationalError then the column names of the
    # query result and the row values are returned
    descript=cur.description
    colnames = [desc[0] for desc in descript]
    row = cur.fetchall()
    return(colnames,row)

def JSON_edition_function(dir_path: str, colnames:list,row:list) -> None:
    """ This function is responsible for editing JSON dictionary, 
        keys representing fields from the query filled with relative 
        results from query rows.
    """

    # setting dictionary keys as query fields
    JSON_dict = dict.fromkeys(colnames)
    
    # filling keys with query results
    for key in JSON_dict.keys():
        JSON_dict[key]=list()
        for j in range(len(row)):
            
            JSON_dict[key].append(row[j][list(JSON_dict.keys()).index(key)])
    
    json_report_file_path = dir_path+'.json'

    # writing dictionary as JSON dump
    with open(json_report_file_path, 'w') as f:
        
        json.dump(JSON_dict, f)

def CSV_edition_function(dir_path: str, colnames:list,row:list) -> None:
    """ This function is responsible for editing csv file, 
        headers representing fields from the query filled with relative 
        results from query rows.
    """

    csv_report_file_path = dir_path+'.csv'
    with open(csv_report_file_path, 'w', newline='') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerow(colnames)
        a.writerows(row)

def HTML_edition_function(dir_path: str, colnames:list,row:list) -> None:
    """ This function is responsible for editing HTML file, 
        headers representing fields from the query filled with relative 
        results from query rows.
    """

    items = ['<table border="1" style="width:auto">', '<tr>']
    # writing table headers
    for k in colnames:
        items.append('<th>%s</th>' % k)
    items.append('</tr>')

    # filling columns with relative row value from query result
    for i in range(len(row)):
        items.append('<tr>')
        for j in range(len(row[i])):
            items.append('<td>%s</td>' % row[i][j])
        items.append('</tr>')

    items.append('</table>')
    test="\n".join(items)
    html_report_file_path = dir_path+'.html'
    with open(html_report_file_path,'w') as html_file:

        html_file.write(test)
    
    
