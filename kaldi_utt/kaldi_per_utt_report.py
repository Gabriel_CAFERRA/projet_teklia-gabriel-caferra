"""This module is in charge of editing the textfiles and csv files
    containing the results of the SQL queries. 
"""
import time
import sqlite3
import logging
import os
from kaldi_utt.CLI_arg_parser import report_arg_parser_function
from kaldi_utt.report_pipeline import report_pipeline_function

def report_edition():
    """The function 'report_edition' sets a timer and logging configuration. 
    It calls for 'report_arg_parser_function' verifying the CLI arguments
    and runs the report_pipeline_function.
    """
    # starting timer
    start = time.time()

    # configurating log as console message
    logging.basicConfig(filename=None, level=logging.DEBUG, 
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
    
    # calling 'report_arg_parser_function' that checks the CLI arguments
    args=report_arg_parser_function()
    
    # getting validated arguments from report_arg_parser_function
    db_con=args[0]
    rep_path=args[1]
    
    # connecting to the database
    TEKLIA_con = sqlite3.connect(db_con)
    
    # the SQL queries are stored in a dictionary where keys are the name
    # of the queries and the values are the relevant full SQLite queries
    
    abs_path=str(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    path_to_the_dict=abs_path+'/SQLqueries_as_dict'

    # running the pipeline
    report_pipeline_function(rep_path,path_to_the_dict,TEKLIA_con)

    # closing database connection
    TEKLIA_con.close()

    # calculating processing time
    end = time.time()
    time_taken = round(float(end-start), 1)
    ms_taken=str(time_taken)
    logging.info(f"{ms_taken}s to run requests and edit reports into {rep_path}")

if __name__ == "__main__":
    report_edition()