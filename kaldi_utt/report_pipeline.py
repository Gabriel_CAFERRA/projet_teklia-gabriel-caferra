""" This module runs the report edition pipeline, 
"""

import sqlite3
import json
from kaldi_utt.queries_edition import query_execution_function,JSON_edition_function,CSV_edition_function,HTML_edition_function

def report_pipeline_function(report_path:str,query_dict_path:str,con:sqlite3.Connection)-> None:
    """ This function gets the report path, the dictionary containing the queries, and connection 
    to the database as given arguments.
    """
    
    # opening the text file containing the queries
    with open(query_dict_path,'r') as sqltext:
        sql_to_dict = sqltext.read()
      
    # reconstructing the data as a dictionary
        sql_as_dict = json.loads(sql_to_dict)

    # running each query from the key values of the dictionary
    for key in sql_as_dict.keys():
        # setting title for each report 
        path_to_report=report_path+'/'+key

        # getting the field names and result from queries
        (col,row)=query_execution_function(sql_as_dict.get(key),con)
        
        # editing reports
        JSON_edition_function(path_to_report,col,row)
        CSV_edition_function(path_to_report,col,row)
        HTML_edition_function(path_to_report,col,row)
        

