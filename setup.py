from setuptools import setup

setup(name='kaldi_utt',
    version='0.1',
    description='Kaldi parser',
    url='https://gitlab.com/teklia/simplon/kaldi-analyzer-gabriel-caferra',
    author='Gabriel CAFERRA',
    author_email='gabriel.caferra.simplon@gmail.com',
    license='TEKLIA',
    packages=['kaldi_utt'],
    entry_points = {
        'console_scripts': ['kaldi-per-utt-parse=kaldi_utt.kaldi_per_utt_parse:main','kaldi-per-utt-report=kaldi_utt.kaldi_per_utt_report:report_edition'],
    },
    zip_safe=False)
