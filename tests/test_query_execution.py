"""This module set tests for the function from the 'parser_functions' module:
    - txt_file_parser
    - word_parser
"""
import pytest
import sqlite3
import json
import os
from kaldi_utt.queries_edition import query_execution_function
from tests.fixtures.dbtest_fixture import db_test, queries_from_dict

# calling the fixture from dbtest_fixture module
@pytest.mark.usefixtures('db_test','queries_from_dict')

def test_query_execution_function_success(db_test,queries_from_dict):
    """Test cases validating that the queries deliver the right answer
    """

    known_words = query_execution_function(queries_from_dict['known_words'], db_test)
    # validating there is 14 words of reference in the test database
    assert len(known_words[1]) == 14

    unknown_words = query_execution_function(
        queries_from_dict['unknown_words'], db_test)
    # there is 20 words of reference in the val and test file but 3 of them
    # are already in the train file
    # validating there is less 20 unknown words in the test database
    assert len(unknown_words[1]) == 17

    unknown_words_yet_recognized = query_execution_function(
        queries_from_dict['unknown_words_yet_recognized'], db_test)
    # from the 20 words of reference in the val and test file
    #  there is 6 errors on val and test file and 3 words that are already known
    assert len(unknown_words_yet_recognized[1]) == (20-6-3)

    error_rate_by_work_reference = query_execution_function(
        queries_from_dict['error_rate_by_work_reference'], db_test)
    error_rate_results = error_rate_by_work_reference[1]

    # there is 7 work references in total
    assert len(error_rate_results) == 7

    # there is an error rate of 1/7 for the work_reference 'balsac/133163/microfilm_CE502S61_-_1878/1971777_r2l1'
    assert float(error_rate_results[5][2]) == round(1/7, 5)

    # fail validate that words are not in the requests

def test_query_execution_function_fail(db_test):
    """Test cases where the requests must raise an error
    """
    
    # testing that a wrong string returns an error
    with pytest.raises(ValueError):
        query_execution_function("blabla", db_test)
