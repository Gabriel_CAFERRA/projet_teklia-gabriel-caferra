""" This module contains fixture used to test request execution
    the path to the test database and the path to the dictionary are
    specified in the code of functions
"""

import pytest
import json
import sqlite3
import os

@pytest.fixture(scope="session")
def db_test():
    conn = sqlite3.Connection('./tests/testfiles/SQLTEST.db')
    yield conn
    conn.close()

@pytest.fixture(scope="function")
def queries_from_dict()-> dict:

# opening the dictionary containing the queries

    with open('./SQLqueries_as_dict', 'r') as sql_text:
        sql_to_dict = sql_text.read()
    # loading the request written in SQLrequests_as_dict
    sql_as_dict = json.loads(sql_to_dict)
    return(sql_as_dict)
