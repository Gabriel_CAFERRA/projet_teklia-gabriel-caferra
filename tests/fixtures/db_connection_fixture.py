import pytest
import sqlite3

@pytest.fixture(scope="session")
def db():
    conn = sqlite3.connect(":memory:")
    yield conn
    conn.close()

@pytest.fixture(scope="function")
def lines_and_tokens_creation(db):
    cur = db.cursor()
    cur.execute(
        """ 
        CREATE TABLE lines(
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "learning_step" STRING,
        "work_reference" STRING,
        "reference_set" STRING,
        "hypotesis_set" STRING,
        "op" STRING,
        "CSID" STRING
        )
        """
        )
    db.commit()
    cur.execute(
        """
        CREATE TABLE tokens (
        "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "word_position" INTEGER,
        "word_of_reference" STRING,
        "word_found" STRING,
        "matching_type" STRING,
        "line_id" INTEGER,
        FOREIGN KEY ("line_id") REFERENCES lines ("id")
        )
        """
        )
    db.commit()
    yield
    cur.execute("""DROP TABLE lines""")
    cur.execute("""DROP TABLE tokens""")
    db.commit()