"""This module set tests for the parsed_txt_file_flattener function from the 'flattener' module
"""
import pytest
from kaldi_utt.flattener import parsed_txt_file_flattener

def test_parsed_txt_file_flattener_success():
    """Test cases where the function must success
    """
    # introducing a dictionary test the matches the right structure with
    # a line number that is a multiple of 4
    dict_test={'learning_step':['val','val','val','val'],
    'work_reference':['ref1','ref1','ref1','ref1'],
    'set_type':['hyp','hyp','hyp','hyp'],
    'set':
    ['this is a ref set',
    'this is a hyp set',
    'this is the CSID line',
    'this is the op line']}
    # validate that text_txt_file_parser returns a dictionary
    assert type(parsed_txt_file_flattener(dict_test))== dict

    # validate that 6 keys are created (with learning_step)
    assert len(parsed_txt_file_flattener(dict_test))== 6
    
    # validate that the keys have the specified names
    keys_list=['learning_step','work_reference','reference_set','hypothesis_set','op', 'CSID']
    assert list(parsed_txt_file_flattener(dict_test).keys()) == keys_list
    
    # validate that each key gets 4 values
    assert (len(parsed_txt_file_flattener(dict_test)['learning_step']) ==
    len(parsed_txt_file_flattener(dict_test)['work_reference']) == 
    len(parsed_txt_file_flattener(dict_test)['reference_set']) ==
    len(parsed_txt_file_flattener(dict_test)['hypothesis_set']) ==
    len(parsed_txt_file_flattener(dict_test)['op']) == 
    len(parsed_txt_file_flattener(dict_test)['CSID']) ==
    1)

def test_parsed_txt_file_flattener_fail():
    """Test cases where the function 'parsed_txt_file_flattener'
        must fail
    """
    with pytest.raises(ValueError):
        
        # introducing a dictionary with irrelevant key names
        irrelevant_key_names={'this is not the correct key name!':
        ['val','val','val','val'],
        'work_reference':
        ['ref1','ref1','ref1','ref1'],
        'set':
        ['this is a ref set',
        'this is a hyp set',
        'this is the CSID line',
        'this is the op line']}
        # testing that a dict with irrelevant key names will raise an error
        parsed_txt_file_flattener(irrelevant_key_names)

        # introducing a dictionary test the matches the right structure with
        # a line number that is not a multiple of 4
        incomplete_dict_test={'learning_step':['val','val','val'],
        'work_reference':['ref1','ref1','ref1'],'set':
        ['this is a ref set',
        'this is a hyp set',
        'a line is missing']}
        # testing that a dict with irrelevant lines number raises an error
        len(parsed_txt_file_flattener(incomplete_dict_test)['set']) 