"""This module set tests for the word_parser function from the 'parser_functions' module
"""
import pytest
import sqlite3
from kaldi_utt.analytic_model import analytical_model_creation
from tests.fixtures.db_connection_fixture import db, lines_and_tokens_creation

# calling the fixture from db_connection_fixture module
@pytest.mark.usefixtures("db",'lines_and_tokens_creation')


def test_analytical_model_creation_success(db,lines_and_tokens_creation):
    """Test cases where the function must success
    """

    # introducing a list of tuple with the suitable structure with 
    # 2 records for each table
    test_tuple_list=(
        [('step1','ref1','ref_set1','hyp_set1','op1','csid1')
        ,('step2','ref2','ref_set2','hyp_set2','op2','csid2')],
        [('w_p1','r_w1','h_w1','m_t1','l_id1')
        ,('w_p2','r_w2','h_w2','m_t2','l_id2')])

    # calling the function to test that returns nothing
    analytical_model_creation(test_tuple_list,db)

    # checking that the tables have correct record number
    
    cur = db.cursor()
    cur.execute("SELECT * FROM lines")
    lines_rows = cur.fetchall()
    print(lines_rows)
    cur.execute("SELECT * FROM tokens")
    tokens_rows = cur.fetchall()
    
    assert len(lines_rows) == 2
    assert len(tokens_rows) == 2
