"""This module set tests for the function from the 'parser_functions' module:
    - txt_file_parser
    - word_parser
"""
import pytest
import sqlite3
from kaldi_utt.analytic_model import tables_creation
from kaldi_utt.parser_functions import word_parser, txt_file_parser
from tests.fixtures.db_connection_fixture import db, lines_and_tokens_creation

# starting tests for 'txt_file_parser' function
def test_txt_file_parser_success():
    """Test cases where the function must success
    """
    # validate that text_txt_file_parser returns a dictionary
    assert type(txt_file_parser('./tests/testfiles/test1.txt')) == dict

    # validate that 4 keys are created (with learning_step)
    assert len(txt_file_parser('./tests/testfiles/test1.txt')) == 4
    # validate that the keys have the specified names
    assert list(txt_file_parser('./tests/testfiles/test1.txt').keys()) == ['learning_step', 'work_reference',
                                                                                'set_type', 'set']
    # validate that each key gets the same number of values
    assert (len(txt_file_parser('./tests/testfiles/test1.txt')['learning_step']) ==
            len(txt_file_parser('./tests/testfiles/test1.txt')['work_reference']) ==
            len(txt_file_parser('./tests/testfiles/test1.txt')['set_type']) ==
            len(txt_file_parser('./tests/testfiles/test1.txt')['set']))

    # validate the keys have 12 values
    assert (len(txt_file_parser('./tests/testfiles/test1.txt')
            ['learning_step']) == 12)

    # validate that the correct values is inserted into the 'set' key
    assert txt_file_parser('./tests/testfiles/test1.txt')['set'][3] == '4 1 1 1'


def test_txt_file_parser_fail():
    """Test cases where the function must fail:
        -a wrong number of lines for the txt file
        -unmatching pattern when applying regular expression 
    """
    with pytest.raises(ValueError):
        
        # testing that the function raises an error when regex doesn't match groups
        txt_file_parser('./tests/testfiles/incompletefile.txt')
        # Testing that a text file with unsuitable line number raises an error
        txt_file_parser('./tests/testfiles/wronglinenumber.txt')

# starting tests for 'word_parser' function

# calling the fixture from db_connection_fixture module
@pytest.mark.usefixtures('db','lines_and_tokens_creation')

def test_word_parser_success(db,lines_and_tokens_creation):
    """Test cases where the function must success
    """

    # introducing a dictionary test that matches the right structure with suitable keys
    dict_test={'learning_step':['val','train','test'],
    'work_reference':['this is a first ref','a second ref','third ref'],
        'reference_set':['it should work','it might work','will it work'],
        'hypothesis_set':['this should work','things might work','things will work'],
        'op':[' C C C','S S S','I I I'], 'CSID':['3 0 0 0','0 3 0 0','0 0 3 0']}
    
    
    test_wp=word_parser(dict_test,db)
    
    # validate that the 'word_parser' function returns a tuple
    
    assert type(test_wp) == tuple
    
    # validate that both element of the returned tuple are a list
    assert type(test_wp[0]) == list
    assert type(test_wp[1]) == list
    
    # validate that each list from the tuple has the same length, 3 tuple records 
    # for the 'lines' table and 9 tuple records for the 'tokens' table corresponding
    # to each word from all sentences 
    assert len(test_wp[0]) == 3
    assert len(test_wp[1]) == 9

    # validate that each list from the list of tuple returned contains tuples
    assert type(test_wp[0][1]) == tuple
    assert type(test_wp[1][2]) == tuple

    # validate that a set of list from the tuple has the same length
    assert len(test_wp[0][0]) == len(test_wp[0][1]) == len(test_wp[0][2]) == 6
    assert len(test_wp[1][0]) == len(test_wp[1][1]) == len(test_wp[1][2]) == 5

def test_word_parser_fail(db,lines_and_tokens_creation):
    # introducing a dictionary test that doesn't match the right structure with suitable keys
    dict_test={'learning_step':['val','train','test'],
    'work_reference':['this is a first ref','a second ref']}
    with pytest.raises(ValueError):
        word_parser(dict_test,db)